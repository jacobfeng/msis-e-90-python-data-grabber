# MSIS-E-90 data grabber

## written by: Feng Lu
## date: June, 05, 2014

This **ipython nootebook** code is used for grabbing the MSIS-E-90 atmosphere modelling data from the website: http://omniweb.gsfc.nasa.gov/vitmo/msis_vitmo.html. 
Please note this is not a simulation code. It just asking the data from gsfc web server.